const _ = require('lodash')
const videoActionsEvaluator = require('./videoActionsEvaluator.js')
const YouTubePlayer = require('youtube-player')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Some arguments are expected')

    if(!(args.player instanceof YouTubePlayer))
        throw new Error('Invalid YoutubePlayer instance')

    if(!_.isObject(args.videoSteps))
        throw new Error('Some video steps are expected')

    if(!(args.videoActionsEvaluator instanceof videoActionsEvaluator))
        throw new Error('Invalid videoActionsEvaluator')

    args.player.setVolume(100)
    args.player.stopVideo()
    args.player.on('stateChange', function (event) {
        var state = args.player.getPlayerState();
        state.then(function (value) {
            args.videoActionsEvaluator.evaluate({
                state: state,
                value: value,
                event: event,
                videoSteps: args.videoSteps
            })
        })
    });
}